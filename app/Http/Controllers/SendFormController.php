<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendForm;
use App\Models\SendForm as SendFormModel;

class SendFormController extends Controller
{

    public function create()

    {
        return view('welcome');
    }

    public function send_form(Request $request)
    {

        $request->validate([
            'full_name' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'comment' => 'required'
        ]);

        $full_name = $request->full_name;
        $phone = $request->phone;
        $city = $request->city;
        $comment = $request->comment;

        SendFormModel::create($request->all());

        Mail::to('testreceiver@gmail.com’')->send(new SendForm($full_name, $phone, $city, $comment));

        return back()->with('success', 'Your form has been submitted.');
    }
}
