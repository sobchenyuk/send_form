<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SendForm extends Model
{
    use HasFactory;

    protected $fillable = [
        'full_name',
        'phone',
        'city',
        'comment',
    ];

    protected $table = 'send_form';
}
